.. Tofu documentation master file, created by
   sphinx-quickstart on Fri Aug 14 17:29:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tofu's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Application Programming Interface
=================================

.. toctree::
   :maxdepth: 2

   api/preprocessing
   api/genreco
   api/util


Usage
=====


.. toctree::
	:maxdepth: 2

	usage/genreco
	usage/flow


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
